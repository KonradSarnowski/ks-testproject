class Model {
  constructor(attr) {
    this.attributes = attr || {}
  }

  set(params) {
    params.each(function(key, value) {
      this.attributes[key] = value
    })
  }
}

export default Model
