import Model from './model'

class Project extends Model {

  static get defaults() {
    return {
      name: 'UNDEFINED'
    }
  }

  static get urlSchema() {
    "/user/:user_id/projects/:id"
  }
}


export default Project
