# es2015-grunt

Purpose: learning ES2015 (aka es6)

## setup

The project uses nodejs, grunt and babel for transpiling es6 to js.

You should install npm first!

   npm install

   grunt

## General usage
If you want to use es6 code then use .es6 extension, if you'd like to add es5 javascript code use standard .js file extension.

## TODO
Feel free to update the project with other grunt settings/modules that would make work easier.
Feel free to write more es6 specific code examples.
Feel fee to add configuration for other module systems (common, umd, system, ignore, custom - take a look at babeljs page to get the idea of how to use it)

Remeber that you can't use every possible es2015 feature for now. Some can be transpiled to js, some can be used with polifills, some, unfortunatelly, are beyond todays javascript possibilities.

General idea is that we can write es2015 code now and transpile it to nowadays javascript and in future simply drop transpiling and just use the es2015 code.

## links
* https://babeljs.io/
* https://babeljs.io/docs/learn-es2015/