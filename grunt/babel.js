module.exports = {
  options: {
    modules: 'amd',
    sourceMap: false
  },
  js: {
    files: [{
      expand: true,
      cwd: 'src/',
      src: ['**/*.es6'],
      dest: 'dist/',
      ext: '.js'
    }]
  }
};
